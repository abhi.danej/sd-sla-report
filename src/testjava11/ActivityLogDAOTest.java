package testjava11;

import java.util.ArrayList;

import activitylog.ActivityLog;
import activitylog.ActivityLogDAO;
import main.Main;

public class ActivityLogDAOTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Main m = new Main();
		m.setupLogging();
		
		ActivityLogDAO alDAO = ActivityLogDAO.getInstance();
		
		ArrayList<ActivityLog> logList = alDAO.getAllLogs();
		
		logList.forEach(l -> Main.logger.info(l.toString()));
	}

}
