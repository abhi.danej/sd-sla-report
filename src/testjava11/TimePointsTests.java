package testjava11;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import activitylog.TimePoints;

public class TimePointsTests {

	public static void main(String[] args) {
		TimePoints respSlaTp = new TimePoints("Response SLA",
				LocalDateTime.of(2018, 10, 12, 11, 0, 0), 
				LocalDateTime.of(2018, 10, 12, 11, 30, 0));
		System.out.println("Response SLA: " + respSlaTp);
		
		TimePoints reslSlaTp = new TimePoints("Resolution SLA",
				LocalDateTime.of(2018, 10, 12, 13, 0, 0), 
				LocalDateTime.of(2018, 10, 12, 11, 30, 0));
		System.out.println("Resolution SLa: " + reslSlaTp);
		
		LocalDateTime t1 = respSlaTp.getStartTime();
		System.out.println(t1);
		
		// if giving epoch seconds ..also give offset
		LocalDateTime t2 = LocalDateTime.ofEpochSecond(1539507160 , 0, ZoneOffset.ofHoursMinutes(5, 30));
		System.out.println(t2);
		
		
		
		System.out.println(reslSlaTp.getStartTime().isAfter(respSlaTp.getStartTime()));
	}
}
