package testjava11;

import java.util.ArrayList;

import main.Main;
import callrequest.CallRequest;
import callrequest.CallRequestDAO;

public class CallRequestDAOTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Main m = new Main();
		m.setupLogging();

		CallRequestDAO crDao = CallRequestDAO.getInstance();
		
		ArrayList<CallRequest> crList = crDao.getCallRequestList();
		
		crList.forEach(cr -> Main.logger.info(cr.toString()));
		
	}

}
