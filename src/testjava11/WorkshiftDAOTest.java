package testjava11;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import workshift.Workshift;
import workshift.WorkshiftDAO;
import callrequest.CallRequest;
import main.Main;

public class WorkshiftDAOTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Main m = new Main();

		m.setupLogging();

		CallRequest cr = new CallRequest("11992524",
		// LocalDateTime.of(2018, 10, 12, 13, 10, 0),
				LocalDateTime.ofEpochSecond(1539769360, 0,
						ZoneOffset.ofHoursMinutes(5, 30)),
				// LocalDateTime.of(2018, 10, 13, 11, 0, 0),
				LocalDateTime.ofEpochSecond(1539770162, 0,
						ZoneOffset.ofHoursMinutes(5, 30)), "S1");
		 Workshift ws = WorkshiftDAO.getInstance().getWorkshift(cr);
		 Main.logger.info(ws.toString());

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:m:sa");
		LocalTime t1 = LocalTime.parse("12:00:00AM", formatter);
		LocalTime t2 = LocalTime.parse("11:59:59PM", formatter);
//		t2= t2.plusMinutes(1L);
//		LocalTime t2 = LocalTime.

		Main.logger.info(t1.toString() + ":" + t2.toString());

		long seconds = ChronoUnit.SECONDS.between(t1, t2);
		Main.logger.info(String.valueOf(seconds));
	}

}
