package testjava11;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import main.Main;
import callrequest.CallRequest;
import commons.Printer;
import workshift.ShiftTimings;
import workshift.Workshift;
import workshift.WorkshiftHelper;

public class WorkshiftTest1 {

	public void parseWorkshift() {
		String data1 = "Mon-Tue{9:30 am-5:30 pm} 10/13/2018{} 10/27/2017{}";
		String data2 = "Fri{9:40 am-5:00 pm} 10/13/2018{} 10/27/2017{}";
		// todo check about sunday?
		
		String data = data1 + data2;
	
		Workshift workshift = new Workshift(12, "Maharashtra");

		for (String value : data.split("}")) {

			value = value.replace(" ", "").toUpperCase();
			System.out.println("Checking Record: " + value);

			if (value.matches("[a-zA-Z]{3}.*\\{\\d{1,2}:\\d{1,2}AM-\\d{1,2}:\\d{1,2}PM.*")) {
				System.out.println("\tParsing Shift timings");

				parseWorkShiftTimings(workshift, value);

			}

			if (value.matches("\\d{1,2}\\/\\d{1,2}\\/\\d{4}\\{")) {
				System.out.println("\tParsing Holidays");

				parseHolidays(workshift, value);

			}
		}

		System.out.println("Workshift Summary: " + workshift.toString());

	}

	/**
	 * @param workshift
	 * @param value
	 */
	private void parseHolidays(Workshift workshift, String value) {
		Pattern weekDay = Pattern.compile("\\d{1,2}\\/\\d{1,2}\\/\\d{4}");

		Matcher m = weekDay.matcher(value);

		// find the start and end days
		while (m.find()) {
			String matched = m.group();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			LocalDate date = LocalDate.parse(matched, formatter);
			System.out.println("\tAdding holiday " + date);
			workshift.addHoliday(date);
		}
	}

	/**
	 * @param workshift
	 * @param value
	 */
	private void parseWorkShiftTimings(Workshift workshift, String value) {

		List<DayOfWeek> daysMatched = new ArrayList<DayOfWeek>();
		Set<Integer> allDaysMatchedValues = new HashSet<Integer>();

		Pattern weekDay = Pattern.compile("[a-zA-Z]{3}");

		Matcher m = weekDay.matcher(value);

		// find the start and end days
		while (m.find()) {

			String matched = m.group();
			System.out.println(matched);

			Stream.of(DayOfWeek.values()).forEach(dow -> {
				if (dow.toString().contains(matched)) {
					daysMatched.add(dow);
					allDaysMatchedValues.add(dow.getValue());
				}
			});
		}

		System.out.println("Size: " + daysMatched.size());
		daysMatched.forEach(day -> System.out.println(day.toString()));

		// parse start and end timings
		Pattern workTimings = Pattern.compile("\\d{1,2}:\\d{1,2}[A|P]M");
		Matcher m2 = workTimings.matcher(value);

		LocalTime shiftStartTime = null;
		LocalTime shiftEndTime = null;
		while (m2.find()) {
			String matched = m2.group();
			System.out.println(matched);

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:ma");
			LocalTime time = LocalTime.parse(matched, formatter);
			if (shiftStartTime == null) {
				shiftStartTime = time;
				System.out.println("Start Time: " + time.toString());
			} else {
				shiftEndTime = time;
				System.out.println("End Time: " + time.toString());
			}
		}
		System.out.println("Biz start and end time: " + shiftStartTime + " to " + shiftEndTime);

		System.out.println("\nDays to consider: " + allDaysMatchedValues.toString());
		
		if (daysMatched.size()==1) {
			workshift.addWorkshiftTimings(daysMatched.get(0), new ShiftTimings(shiftStartTime, shiftEndTime));
		}
		if (daysMatched.size()==2) {
			int startDayValue = daysMatched.get(0).getValue();
			int endDayValue = daysMatched.get(1).getValue();
			
			for(DayOfWeek day: DayOfWeek.values()) {
				if( day.getValue() >= startDayValue && day.getValue() <= endDayValue)
					workshift.addWorkshiftTimings(day, new ShiftTimings(shiftStartTime, shiftEndTime));
				
			}
			
		}
		
		

	}

	public static void main(String[] args) {
		
		Main m = new Main();
		m.setupLogging();
		
		System.out.println("Start at: " + LocalDateTime.now());
		WorkshiftTest1 t = new WorkshiftTest1();
		// t.run();
		// open: 10/11/2018 @ 9:45am (UTC)
		// close: 10/13/2018 @ 10:10am (UTC)
		CallRequest c1 = new CallRequest(
				"CR1234", 
				LocalDateTime.ofEpochSecond(1539251100,0, ZoneOffset.ofHoursMinutes(5, 30)), 
				LocalDateTime.ofEpochSecond(1539425344,0, ZoneOffset.ofHoursMinutes(5, 30)), 
				"PRiority-High");
		WorkshiftHelper wsh = new WorkshiftHelper(c1);
		wsh.populateWorkshift();
		
	}
	

 	public void run() {

		String a = "Abhishek";
		System.out.println(a);
		Printer.print("hey");

		ArrayList<String> list = new ArrayList<String>();

		list.add("abhishek");
		list.add("Danej");
		List<String> k = list.stream().filter(x -> x.startsWith("a")).collect(Collectors.toList());
		System.out.println(k);

		// 1534960278
		LocalDateTime dt1 = LocalDateTime.ofInstant(Instant.ofEpochSecond(1535150008), ZoneId.systemDefault());
		System.out.println(dt1 + ", " + dt1.getDayOfYear());

		DayOfWeek d = dt1.getDayOfWeek();
		System.out.println(d);

		LocalDateTime dt2 = LocalDateTime.ofInstant(Instant.ofEpochSecond(1535160278), ZoneId.systemDefault());
		System.out.println(dt2 + ", " + dt2.getDayOfYear());

		System.out.println(dt1.isBefore(dt2));

		System.out.println(LocalDateTime.parse("2018-08-16T09:41:18").getDayOfYear());

		LocalDate ld1 = dt1.toLocalDate();
		LocalDate ld2 = dt2.toLocalDate();
		System.out.println(ld1);

		int daysPeriod = Period.between(ld1, ld2).getDays();
		System.out.println("Days between " + dt1 + " and " + dt2 + ": " + daysPeriod);

		// int gap = ChronoUnit.DAYS.be
		for (int i = 0; i <= 0; i++) {
			System.out.println("OK");
		}

	}

}
