package testjava11;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import activitylog.ActivityLogHelper;
import callrequest.CallRequest;
import callrequest.StatusHelper;
import main.Main;

public class ActivityLogHelperTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Main m = new Main();
		m.setupLogging();
		StatusHelper sHelper = new StatusHelper();
		
		CallRequest cr = new CallRequest("cr:12433290", 
//				LocalDateTime.of(2018, 10, 12, 13, 10, 0),
				LocalDateTime.ofEpochSecond(1539769360, 0, ZoneOffset.ofHoursMinutes(5, 30)),
//				LocalDateTime.of(2018, 10, 13, 11, 0, 0), 
				LocalDateTime.ofEpochSecond(1539770162, 0, ZoneOffset.ofHoursMinutes(5, 30)),
				"S1");

		ActivityLogHelper helper = new ActivityLogHelper(cr);
		
		helper.process();
	}

}
