package workshift;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import main.Main;
import callrequest.CallRequest;

public class WorkshiftDAO {

	//
	// SELECT TOP 100 dbo.pri.sym, dbo.pri.service_type, dbo.call_req.ref_num,
	// dbo.bpwshft.sym AS workshift, dbo.bpwshft.description, dbo.bpwshft.sched
	// FROM dbo.call_req INNER JOIN
	// dbo.pri ON dbo.call_req.priority = dbo.pri.enum LEFT OUTER JOIN
	// dbo.bpwshft INNER JOIN
	// dbo.evt ON dbo.bpwshft.persid = dbo.evt.work_shift INNER JOIN
	// dbo.slatpl ON dbo.evt.persid = dbo.slatpl.event ON dbo.pri.service_type =
	// dbo.slatpl.service_type
	// where dbo.call_req.ref_num='11992524'
	// order by dbo.call_req.id desc

	private static WorkshiftDAO wsDAO = new WorkshiftDAO();

	private WorkshiftDAO() {

	}

	public static WorkshiftDAO getInstance() {
		return wsDAO;
	}

	public Workshift getWorkshift(CallRequest request) {
		// db access code goes here.

		Workshift ws = null;

		try {
			Main.logger.info("called");
			String connectionURL = "jdbc:sqlserver://HBUSDUATDB_NEW:1400;database=mdb;user=servicedesk;password=wipro@123";

			String sql = "SELECT TOP 100 dbo.bpwshft.id, dbo.call_req.ref_num, dbo.bpwshft.sym AS workshift, dbo.bpwshft.description, dbo.bpwshft.sched"
					+ " FROM"
					+ " dbo.call_req INNER JOIN "
					+ " dbo.pri ON dbo.call_req.priority = dbo.pri.enum LEFT OUTER JOIN"
					+ " dbo.bpwshft INNER JOIN"
					+ " dbo.evt ON dbo.bpwshft.persid = dbo.evt.work_shift INNER JOIN"
					+ " dbo.slatpl ON dbo.evt.persid = dbo.slatpl.event ON dbo.pri.service_type = dbo.slatpl.service_type"
					+ " where dbo.call_req.ref_num='"
					+ request.getRef_num()
					+ "'" 
					+ " and dbo.slatpl.object_type='cr'"
					+ " order by dbo.call_req.id desc";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(connectionURL);
			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(sql);
			Main.logger
					.info("SQL Select command executed successfully and return the resultset. SQL: " + sql);

			rs.next();
//			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
//				Main.logger.info(rs.getString(1) + ": " + rs.getString(2)
//						+ ": " + rs.getString(3) + ": " + rs.getString(4)
//						+ ": " + rs.getString(5)
//						);

					String sched = rs.getString(5);

					ws = populateWorkshift(request, id, name, sched);
//			}
			rs.close();
			con.close();

		} catch (Exception e) {
			Main.logger.info(e.getMessage());
			Main.logger.info("SQL FAILED: Select command execution failed, Reason - "
					+ e.getMessage());
		}

		return ws;
	}

	public Workshift populateWorkshift(CallRequest request, int id, String name, String sched) {

		Workshift workshift = new Workshift(id, name);
		Main.logger.info("Schedule: " + sched);
		for (String value : sched.split("}")) {

			value = value.replace(" ", "").toUpperCase();
			Main.logger.fine("Checking Record: " + value);

			if (value.matches("[a-zA-Z]{3}.*\\{\\d{1,2}:\\d{1,2}(AM|PM)-\\d{1,2}:\\d{1,2}(AM|PM).*")) {
				Main.logger.fine("\tParsing Shift Days and timings");

				parseWorkShiftTimings(workshift, value);

			}

			if (value.matches("\\d{1,2}\\/\\d{1,2}\\/\\d{4}\\{")) {
				Main.logger.fine("\tParsing Holidays");

				parseHolidays(workshift, value);

			}
		}

		Main.logger.info("Workshift Summary: " + workshift.toString());

		return workshift;
	}
	
	private void parseHolidays(Workshift workshift, String value) {
		Pattern weekDay = Pattern.compile("\\d{1,2}\\/\\d{1,2}\\/\\d{4}");

		Matcher m = weekDay.matcher(value);

		// find the start and end days
		while (m.find()) {
			String matched = m.group();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			LocalDate date = LocalDate.parse(matched, formatter);
			Main.logger.fine("\tAdding as holiday " + date);
			workshift.addHoliday(date);
		}
	}
	
	private void parseWorkShiftTimings(Workshift workshift, String value) {

		List<DayOfWeek> daysMatched = new ArrayList<DayOfWeek>();
		Set<Integer> allDaysMatchedValues = new HashSet<Integer>();

		Pattern weekDay = Pattern.compile("[a-zA-Z]{3}");

		Matcher m = weekDay.matcher(value);

		// find the start and end days
		while (m.find()) {

			String matched = m.group();
			Main.logger.finer("Found Day: " + matched);

			Stream.of(DayOfWeek.values()).forEach(dow -> {
				if (dow.toString().contains(matched)) {
					daysMatched.add(dow);
					allDaysMatchedValues.add(dow.getValue());
				}
			});
		}

//		Main.logger.info("Size: " + daysMatched.size());

		// daysMatched.sort((d1, d2) -> d1.getValue() - d2.getValue());

		daysMatched.forEach(day -> Main.logger.info("Found Shift day: " + day.toString()));

		// parse start and end timings
		Pattern workTimings = Pattern.compile("\\d{1,2}:\\d{1,2}[A|P]M");
		Matcher m2 = workTimings.matcher(value);

		LocalTime shiftStartTime = null;
		LocalTime shiftEndTime = null;
		while (m2.find()) {
			String matched = m2.group();
			Main.logger.finer(matched);

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:ma");
			LocalTime time = LocalTime.parse(matched, formatter);
			if (shiftStartTime == null) {
				shiftStartTime = time;
				Main.logger.finer("Shift Start Time: " + time.toString());
			} else {
				shiftEndTime = time;
				Main.logger.finer("Shift End Time: " + time.toString());
			}
		}
		if(shiftEndTime.equals(LocalTime.MIN)){
			shiftEndTime=LocalTime.MAX;
		}
		Main.logger.fine("Stored Biz start and end time: " + shiftStartTime + " to " + shiftEndTime);

		Main.logger.fine("Days to consider: " + allDaysMatchedValues.toString());

		if (daysMatched.size() == 1) {
			workshift.addWorkshiftTimings(daysMatched.get(0), new ShiftTimings(shiftStartTime, shiftEndTime));
		}
		if (daysMatched.size() == 2) {
//			int startDayValue = daysMatched.get(0).getValue();
//			int endDayValue = daysMatched.get(1).getValue();

//			for (DayOfWeek day : DayOfWeek.values()) {
//				Main.logger.finer("Testing day: " + day.toString());
//				if (day.getValue() >= startDayValue && day.getValue() <= endDayValue)
//					workshift.addWorkshiftTimings(day, new ShiftTimings(shiftStartTime, shiftEndTime));
//			}
			
//			for(DayOfWeek day = daysMatched.get(0); 
//					daysMatched.get(0).getValue() <= daysMatched.get(1).plus(1L).getValue(); 
//					day = day.plus(1L)) {
//				Main.logger.info("Testing: " + day);
//				workshift.addWorkshiftTimings(day, new ShiftTimings(shiftStartTime, shiftEndTime));
//			}
			
			DayOfWeek day = daysMatched.get(0);
			do {
				workshift.addWorkshiftTimings(day, new ShiftTimings(shiftStartTime, shiftEndTime));
				day = day.plus(1L);
			} while (! day.equals(daysMatched.get(1).plus(1L)));	
			
		}
	}

}
