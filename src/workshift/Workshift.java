package workshift;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.HashSet;

import main.Main;

public class Workshift {

	private int id;
	private String name;
	
	private HashSet<LocalDate> holidaySet = new HashSet<>();
	private HashMap<DayOfWeek, ShiftTimings> workshiftTimings = new HashMap<>();
	
	public Workshift(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public HashSet<LocalDate> getHolidaySet() {
		return holidaySet;
	}
	
	public boolean isHoliday(LocalDate date) {
		if (holidaySet.contains(date)) {
			return true;
			}
		else {
			return false;
		}
	}

//	public void setHolidaySet(HashSet<LocalDate> holidaySet) {
//		this.holidaySet = holidaySet;
//	}
	
	public void addHoliday(LocalDate holiday) {
		this.holidaySet.add(holiday);
	}

	public HashMap<DayOfWeek, ShiftTimings> getWorkshiftTimings() {
		return workshiftTimings;
	}
	
	public LocalTime getShiftStartTimeForDate(LocalDate date) {
		DayOfWeek day = date.getDayOfWeek();
		if(this.workshiftTimings.containsKey(day)) {
			return this.workshiftTimings.get(day).getShiftStartTime();			
		} else {
			Main.logger.warning("Workshift timings unavailable for date " + date
					+ " which was " + day);
			return null;
		}
	}
	
	public LocalTime getShiftEndTimeForDate(LocalDate date) {
		DayOfWeek day = date.getDayOfWeek();
		if(this.workshiftTimings.containsKey(day)) {
			return this.workshiftTimings.get(day).getShiftEndTime();			
		} else {
			Main.logger.warning("Workshift timings unavailable for date " + date
					+ " which was " + day);
			return null;
		}
	}

//	public void setWorkshiftTimings(HashMap<DayOfWeek, ShiftTimings> workshiftTimings) {
//		this.workshiftTimings = workshiftTimings;
//	}
	
	public void addWorkshiftTimings(DayOfWeek dow, ShiftTimings shiftTimings) {
		this.workshiftTimings.put(dow, shiftTimings);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Workshift [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", holidaySet=");
		builder.append(holidaySet);
		builder.append(", workshiftTimings=");
		builder.append(workshiftTimings);
		builder.append("]");
		return builder.toString();
	}
	
	
}
