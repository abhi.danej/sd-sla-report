package workshift;

import java.time.LocalTime;

public class ShiftTimings {

	private final LocalTime shiftStartTime;
	private final LocalTime shiftEndTime;
	
	public ShiftTimings(LocalTime shiftStartTime, LocalTime shiftEndTime) {
		super();
		this.shiftStartTime = shiftStartTime;
		this.shiftEndTime = shiftEndTime;
	}

	public LocalTime getShiftStartTime() {
		return shiftStartTime;
	}

	public LocalTime getShiftEndTime() {
		return shiftEndTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ShiftTimings [shiftStartTime=");
		builder.append(shiftStartTime);
		builder.append(", shiftEndTime=");
		builder.append(shiftEndTime);
		builder.append("]");
		return builder.toString();
	}
	
}
