package workshift;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import callrequest.CallRequest;
import main.Main;

public class WorkshiftHelper {

	CallRequest request;

	public WorkshiftHelper(CallRequest request) {
		this.request = request;
	}

	private void parseWorkShiftTimings(Workshift workshift, String value) {

		List<DayOfWeek> daysMatched = new ArrayList<DayOfWeek>();
		Set<Integer> allDaysMatchedValues = new HashSet<Integer>();

		Pattern weekDay = Pattern.compile("[a-zA-Z]{3}");

		Matcher m = weekDay.matcher(value);

		// find the start and end days
		while (m.find()) {

			String matched = m.group();
			Main.logger.finer("Found Day: " + matched);

			Stream.of(DayOfWeek.values()).forEach(dow -> {
				if (dow.toString().contains(matched)) {
					daysMatched.add(dow);
					allDaysMatchedValues.add(dow.getValue());
				}
			});
		}

//		Main.logger.info("Size: " + daysMatched.size());

		// daysMatched.sort((d1, d2) -> d1.getValue() - d2.getValue());

		daysMatched.forEach(day -> Main.logger.info("Found Shift day: " + day.toString()));

		// parse start and end timings
		Pattern workTimings = Pattern.compile("\\d{1,2}:\\d{1,2}[A|P]M");
		Matcher m2 = workTimings.matcher(value);

		LocalTime shiftStartTime = null;
		LocalTime shiftEndTime = null;
		while (m2.find()) {
			String matched = m2.group();
			Main.logger.finer(matched);

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:ma");
			LocalTime time = LocalTime.parse(matched, formatter);
			if (shiftStartTime == null) {
				shiftStartTime = time;
				Main.logger.finer("Shift Start Time: " + time.toString());
			} else {
				shiftEndTime = time;
				Main.logger.finer("Shift End Time: " + time.toString());
			}
		}
		Main.logger.fine("Stored Biz start and end time: " + shiftStartTime + " to " + shiftEndTime);

		Main.logger.fine("Days to consider: " + allDaysMatchedValues.toString());

		if (daysMatched.size() == 1) {
			workshift.addWorkshiftTimings(daysMatched.get(0), new ShiftTimings(shiftStartTime, shiftEndTime));
		}
		if (daysMatched.size() == 2) {
			int startDayValue = daysMatched.get(0).getValue();
			int endDayValue = daysMatched.get(1).getValue();

//			for (DayOfWeek day : DayOfWeek.values()) {
//				Main.logger.finer("Testing day: " + day.toString());
//				if (day.getValue() >= startDayValue && day.getValue() <= endDayValue)
//					workshift.addWorkshiftTimings(day, new ShiftTimings(shiftStartTime, shiftEndTime));
//			}
			
//			for(DayOfWeek day = daysMatched.get(0); 
//					daysMatched.get(0).getValue() <= daysMatched.get(1).plus(1L).getValue(); 
//					day = day.plus(1L)) {
//				Main.logger.info("Testing: " + day);
//				workshift.addWorkshiftTimings(day, new ShiftTimings(shiftStartTime, shiftEndTime));
//			}
			
			DayOfWeek day = daysMatched.get(0);
			do {
				workshift.addWorkshiftTimings(day, new ShiftTimings(shiftStartTime, shiftEndTime));
				day = day.plus(1L);
			} while (! day.equals(daysMatched.get(1).plus(1L)));	
			
		}
	}

	private void parseHolidays(Workshift workshift, String value) {
		Pattern weekDay = Pattern.compile("\\d{1,2}\\/\\d{1,2}\\/\\d{4}");

		Matcher m = weekDay.matcher(value);

		// find the start and end days
		while (m.find()) {
			String matched = m.group();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			LocalDate date = LocalDate.parse(matched, formatter);
			Main.logger.fine("\tAdding as holiday " + date);
			workshift.addHoliday(date);
		}
	}

	private String get(String crNumber) {
		String data1 = "Mon - Fri { 8:00 am - 8:00 pm} 10/14/2018{} 10/27/2017{}";
		String data2 = "Sat { 8:00 am - 8:00 pm} 10/14/2018{} 10/27/2017{}";
		String data3 = "Sun - Sat { 12:00 am - 12:00 pm} 10/14/2018{} 10/27/2017{}";
		String data4 = "Sat - Mon { 8:00 am - 8:00 pm} 10/14/2018{} 10/27/2017{}";
		// todo check about sunday?

		return data3;

	}

	public void populateWorkshift() {
		
		Workshift ws = WorkshiftDAO.getInstance().getWorkshift(request);
		request.setWorkshift(ws);

//		String data = get(request.getId());
//		Workshift workshift = new Workshift(12, "Maharashtra");
//
//		for (String value : data.split("}")) {
//
//			value = value.replace(" ", "").toUpperCase();
//			Main.logger.fine("Checking Record: " + value);
//
//			if (value.matches("[a-zA-Z]{3}.*\\{\\d{1,2}:\\d{1,2}AM-\\d{1,2}:\\d{1,2}PM.*")) {
//				Main.logger.fine("\tParsing Shift Days and timings");
//
//				parseWorkShiftTimings(workshift, value);
//
//			}
//
//			if (value.matches("\\d{1,2}\\/\\d{1,2}\\/\\d{4}\\{")) {
//				Main.logger.fine("\tParsing Holidays");
//
//				parseHolidays(workshift, value);
//
//			}
//		}
//
//		Main.logger.info("Workshift Summary: " + workshift.toString());
//

	}
}
