package callrequest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;

import main.Main;

public class CallRequestDAO {

	private static CallRequestDAO crDAO = new CallRequestDAO();
	private ArrayList<CallRequest> crList = null;

	private CallRequestDAO() {

	}

	public static CallRequestDAO getInstance() {
		return crDAO;
	}

	public ArrayList<CallRequest> getCallRequestList(String ref_num) {
		// db access code goes here.
		crList = new ArrayList<>();

		try {
			Main.logger.info("called");
			String connectionURL = "jdbc:sqlserver://HBUSDUATDB_NEW:1400;database=mdb;user=servicedesk;password=wipro@123";
			
			String sql = "Select top 100 "
					+ " ref_num, persid, open_date, resolve_date, close_date, priority "
					+ " from call_req"
					+ " where ref_num='" + ref_num + "'";
//			String sql = "Select * from zadd_call_req where zdel_flag=0 order by id asc";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(connectionURL);
			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(sql);
			Main.logger.info("SQL Select command executed successfully and return the resultset.");
			
			while(rs.next())
			{
				Main.logger.finer(
							rs.getString(1) + ": " 
							+ rs.getString(2) + ": " 
							+ rs.getString(3) + ": " 
							+ rs.getString(4) + ": "
//							+ rs.getString(5) 
							);
				
				CallRequest cr = new CallRequest( 
						rs.getString(1),
						rs.getString(2),
						LocalDateTime.ofEpochSecond(rs.getLong(3), 0, ZoneOffset.ofHoursMinutes(5, 30)),
						LocalDateTime.ofEpochSecond(rs.getLong(4), 0, ZoneOffset.ofHoursMinutes(5, 30)),
						LocalDateTime.ofEpochSecond(rs.getLong(5), 0,ZoneOffset.ofHoursMinutes(5, 30)), 
						rs.getString(6));
				crList.add(cr);
				
			}
			rs.close();
			con.close();
			
		} catch (Exception e) {
			Main.logger.info(e.getMessage());
			Main.logger.info("SQL Select command execution failed, Reason - "
					+ e.getMessage());
			
//			CallRequest cr = new CallRequest("cr:12433290",
//					LocalDateTime.ofEpochSecond(1539769360, 0,
//							ZoneOffset.ofHoursMinutes(5, 30)),
//							LocalDateTime.ofEpochSecond(1539770147, 0,
//									ZoneOffset.ofHoursMinutes(5, 30)), "Priority-52");
//			crList.add(cr);

		}

		return crList;
	}
}
