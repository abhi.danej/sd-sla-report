package callrequest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import activitylog.TimePoints;
import workshift.Workshift;

public class CallRequest {

	String serviceType;

	private final String persid;
	private String rel_attr;
	private String common_name;
	private final String ref_num;
	private LocalDateTime openDateTime;
	private LocalDateTime resolveDateTime;
	private LocalDateTime closeDateTime;
	private String priority;

	/*
	 * FIELDS FOR REPORTING
	 */
	private Workshift workshift;
	private TimePoints responseTPs;
	private TimePoints resolutionTPs;
	private ArrayList<TimePoints> holdTPList;
	
	long responseDuration = -1;
	long resolutionDuration = -1;
	long holdDuration = -1;

	public CallRequest(String ref_num, String persid, LocalDateTime openDateTime, LocalDateTime resolveDateTime, LocalDateTime closeDateTime, String priority) {
		this.ref_num = ref_num;
		this.persid = persid;
		this.openDateTime = openDateTime;
		this.resolveDateTime = resolveDateTime;
		this.closeDateTime = closeDateTime;
		this.priority = priority;
	}

	public String getPersid() {
		return persid;
	}

	public LocalDateTime getOpenTime() {
		return openDateTime;
	}

	public void setOpenTime(LocalDateTime openDateTime) {
		this.openDateTime = openDateTime;
	}

	public LocalDateTime getCloseTime() {
		return closeDateTime;
	}

	public void setCloseTime(LocalDateTime closeDateTime) {
		this.closeDateTime = closeDateTime;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Workshift getWorkshift() {
		return workshift;
	}

	public void setWorkshift(Workshift workshift) {
		this.workshift = workshift;
	}

	public LocalDateTime getResolveDateTime() {
		return resolveDateTime;
	}

	public void setResolveDateTime(LocalDateTime resolveDateTime) {
		this.resolveDateTime = resolveDateTime;
	}

	public TimePoints getResolutionTPs() {
		return resolutionTPs;
	}

	public void setResolutionTPs(TimePoints resolutionTPs) {
		this.resolutionTPs = resolutionTPs;
	}

	public ArrayList<TimePoints> getHoldTPList() {
		return holdTPList;
	}

	public void setHoldTPList(ArrayList<TimePoints> holdTPList) {
		this.holdTPList = holdTPList;
	}

	public TimePoints getResponseTPs() {
		return responseTPs;
	}

	public void setResponseTPs(TimePoints responseTPs) {
		this.responseTPs = responseTPs;
	}

	public long getHoldDuration() {
		return holdDuration;
	}

	public void setHoldDuration(long holdDuration) {
		this.holdDuration = holdDuration;
	}

	public long getResponseDuration() {
		return responseDuration;
	}

	public void setResponseDuration(long responseDuration) {
		this.responseDuration = responseDuration;
	}

	public long getResolutionDuration() {
		return resolutionDuration;
	}

	public void setResolutionDuration(long resolutionDuration) {
		this.resolutionDuration = resolutionDuration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CallRequest [persid=");
		builder.append(persid);
		builder.append(", ref_num=");
		builder.append(ref_num);
		builder.append(", openDateTime=");
		builder.append(openDateTime);
		builder.append(", resolveDateTime=");
		builder.append(resolveDateTime);
		builder.append(", closeDateTime=");
		builder.append(closeDateTime);
		builder.append("]");
		return builder.toString();
	}

	public String getRef_num() {
		return ref_num;
	}

}
