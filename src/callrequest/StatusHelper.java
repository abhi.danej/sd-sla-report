package callrequest;

import java.util.HashSet;

import main.Main;

public class StatusHelper {
	
	private static HashSet<Status> statusSet = new HashSet<>();
	
	public StatusHelper() {
		if(statusSet.isEmpty()) {
			this.get();
		}
	}
	private void get() {
		Status s1 = new Status(1, "Open", "OP", false, false);
		Status s2 = new Status(5201, "Closed", "OP", true, true);
		Status s3 = new Status(2, "Acknowledged", "ACK", false, false);
		Status s4 = new Status(3, "Work In Proress", "WIP", false, false);
		Status s5 = new Status(4, "User Pending", "UP", true, false);
		Status s6 = new Status(5, "Vendor Pending", "VP", true, false);
		Status s7 = new Status(6, "Technically Resolved", "TR", true, true);
		
		statusSet.add(s1);
		statusSet.add(s2);
		statusSet.add(s3);
		statusSet.add(s4);
		statusSet.add(s5);
		statusSet.add(s6);
		statusSet.add(s7);
		
		Main.logger.info("status populated: ");
		statusSet.forEach(s -> Main.logger.info("Status: " + s));
	}
	
	public static HashSet<Status> getStatusSet() {
		return statusSet;
	}
}
