package callrequest;

public class Status {

	private int id;
	private String name;
	private String code;
	private boolean isHold = false;
	private boolean isResolve = false;
	
	public Status(int id, String name, String code, boolean isHold, boolean isResolve) {
		this.id = id;
		this.name = name;
		this.code = code;
		this.isHold = isHold;
		this.isResolve = isResolve;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}


	public String getCode() {
		return code;
	}

	public boolean isHold() {
		return isHold;
	}

	public boolean isResolve() {
		return isResolve;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Status [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", code=");
		builder.append(code);
		builder.append(", isHold=");
		builder.append(isHold);
		builder.append(", isResolve=");
		builder.append(isResolve);
		builder.append("]");
		return builder.toString();
	}
}
