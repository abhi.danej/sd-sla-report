package main;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Logger;

import activitylog.ActivityLogHelper;
import activitylog.TimePoints;
import callrequest.CallRequest;
import callrequest.CallRequestDAO;
import workshift.Workshift;
import workshift.WorkshiftHelper;
import commons.LogManager;
import commons.PerfMon;

public class Main {

	public static Logger logger = null; // new LogManager().getLogger(new Properties());

//	ArrayList<ActivityLog> logList = new ArrayList<>();
	ActivityLogHelper actLogHelper = null;

//	private HashMap<String, TimePoints> crRspSLAMap = new HashMap<>();
//	private HashMap<String, TimePoints> crRslSLAMap = new HashMap<>();
//	private HashMap<String, ArrayList<TimePoints>> crHoldSLAMap = new HashMap<>();
//	private HashMap<String, Workshift> crWorkshiftMap = new HashMap<>();

	public static void main(String[] args) {

		Main m = new Main();

		
//		CallRequest cr = new CallRequest("cr:12433290", 
////				LocalDateTime.of(2018, 10, 12, 13, 10, 0),
//				LocalDateTime.ofEpochSecond(1539769360, 0, ZoneOffset.ofHoursMinutes(5, 30)),
////				LocalDateTime.of(2018, 10, 13, 11, 0, 0), 
//				LocalDateTime.ofEpochSecond(1539770162, 0, ZoneOffset.ofHoursMinutes(5, 30)),
//				"S1");

		//m.run("11991996");  
		//m.run("11992524");
		//m.run("11989658");
		m.run("11907804");
	}

	private void run(String ref_num) {
		
		
//		ArrayList<CallRequest> crList = CallRequestDAO.getInstance().getCallRequestList();

		setupLogging();
		logger.info("Proceeding..");
		PerfMon.getMemStats();
		
		CallRequest request = processCallRequest1(ref_num);
		Main.logger.info("Working on CR: " + request.toString());
		
		processActLogs(request);
		PerfMon.getMemStats();
//		prepareSLATimePoints(request);
		prepareWorkshiftData(request);
		PerfMon.getMemStats();
		performCalculations(request);
		PerfMon.getMemStats();
		logger.info("App Finished Processing");

	}

	public void setupLogging() {
		try {
			logger = (new LogManager()).getLogger(new Properties());
			// System.out.println("SETTING UP LOG");
			logger.info("log setup completed");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private CallRequest processCallRequest1(String ref_num) {
		CallRequestDAO crDAO = CallRequestDAO.getInstance();
		ArrayList<CallRequest> crList = crDAO.getCallRequestList(ref_num);
		return crList.get(0);
	}
	
	private void prepareWorkshiftData(CallRequest request) {
		WorkshiftHelper wsHelper = new WorkshiftHelper(request);
		wsHelper.populateWorkshift();
//		crWorkshiftMap.put(request, wsHelper.getWorkshift());
	}

	private void performCalculations(CallRequest request) {
//		String crNumber = request.getId();
		Main.logger.info("\nPERFORM CALULCATIONS\n");

		Workshift ws = request.getWorkshift();
		LocalDate openDate = request.getOpenTime().toLocalDate();
		LocalDate closeDate = request.getCloseTime().toLocalDate();

		StringBuilder summary = new StringBuilder();
		summary
			.append("Open Time: ")
			.append(request.getOpenTime());
		try {
			summary.append(", Response Time: ")
			.append(request.getResponseTPs().getEndTime());
		} catch (Exception e) {
			Main.logger.warning("Response Timings Not Available");
		}
			summary.append(", Resolution Time: ")
			.append(request.getResolutionTPs().getEndTime())
			.append(", SLAHold times: ")
			.append(request.getHoldTPList());
		logger.info("Request Summary: " + summary.toString());

		int days = (int) ChronoUnit.DAYS.between(openDate, closeDate);
		logger.info("Calendar Days between OPEN and CLOSE : " + days);

		long rspSeconds = 0;
		long rslSeconds = 0;
		long holdSeconds = 0;

		LocalDate date = openDate;

		for (int i = 0; i <= days; i++) {

			logger.info("");
			logger.info("\tCalculation Date: " + date + ", " + date.getDayOfWeek());

//			LocalTime slaStartTime = null;
//			LocalTime slaEndTime = null;

			long todayRslSeconds = 0;
			long todayRspSeconds = 0;
			long todayHoldSeconds = 0;

			final LocalTime bizStartTime = ws.getShiftStartTimeForDate(date);
			final LocalTime bizEndTime = ws.getShiftEndTimeForDate(date);

			if (bizStartTime == null) {
				logger.info("Shift Timings unavailable for : " + date);
				date = date.plusDays(1);
				logger.info("Proceeding to next date.");
				continue;
			} else {
				logger.info("Shift timings: " + bizStartTime + " - " + bizEndTime);
			}

			if (ws.isHoliday(date)) {
				logger.info(date + " is a Holiday. Proceeding to next date.");
				date = date.plusDays(1);
				continue;
			} else {
				logger.info(date + " is not a Holiday. Proceeding with calculation.");
			}

			StringBuilder message = new StringBuilder();

			/*
			 * 
			 * RESOLVE SLA CALCULATION BEGIN
			 * 
			 */

			/*
			 * CALCULATE START TIME
			 */

			/*
			 * if (openDate.equals(date)) {
			 * 
			 * // if (ws.isHoliday(crOpenDate)) { // logger.info(crNumber +
			 * " was Opened on " + crOpenDateTime + " holiday. Moving to next day."); //
			 * continue; // } // logger.info(date + " is not a holiday.");
			 * 
			 * if (openTime.isBefore(bizStartTime)) { slaStartTime = bizStartTime;
			 * logger.info(crNumber + " was opened before start of Shift hours."); }
			 * 
			 * else if (openTime.isAfter(bizStartTime) && openTime.isBefore(bizEndTime)) {
			 * logger.info(crNumber + " was opened in Shift hours."); slaStartTime =
			 * openTime; }
			 * 
			 * else if (openTime.isAfter(bizEndTime)) { logger.info(crNumber +
			 * " was opened after Shift hours. Moving to next day."); continue; } }
			 * 
			 * if (slaStartTime == null) { slaStartTime = bizStartTime; }
			 * logger.info("Resolution SLA calculation start time: " + slaStartTime);
			 * 
			 * 
			 * CALC END TIME
			 * 
			 * 
			 * if (resolveDate.equals(date)) {
			 * 
			 * // if (ws.isHoliday(crResolveDate)) { // logger.info( // crNumber +
			 * " was Resolved on " + crResolveDateTime +
			 * " holiday. Proceeding to next day."); // continue; // } // logger.info(date +
			 * " is not a holiday.");
			 * 
			 * if (resolveTime.isBefore(bizStartTime)) { slaEndTime = bizStartTime;
			 * logger.info(crNumber + " was Resolved before start of Shift hours."); }
			 * 
			 * else if (resolveTime.isAfter(bizStartTime) &&
			 * resolveTime.isBefore(bizEndTime)) { slaEndTime = resolveTime;
			 * logger.info(crNumber + " was Resolved in Shift hours."); }
			 * 
			 * else if (resolveTime.isAfter(bizEndTime)) { slaEndTime = bizEndTime;
			 * logger.info(crNumber + " was Resolved after Shift hours."); }
			 * 
			 * }
			 * 
			 * if (slaEndTime == null) { slaEndTime = bizEndTime; }
			 * logger.info("Resolution SLA calculation end time: " + slaEndTime);
			 * 
			 * todayRslSeconds = ChronoUnit.SECONDS.between(slaStartTime, slaEndTime);
			 */

			todayRslSeconds = calcResolutionTimeForDate(request, date);
			rslSeconds = rslSeconds + todayRslSeconds;

			message.append("\n\t");
			message.append("On " + date + ", time spent in resolution (seconds): " + todayRslSeconds + ", "
					+ getHMSfromSeconds(todayRslSeconds));
			/*
			 * 
			 * RESOLVE SLA CALCULATION END
			 * 
			 */

			/*
			 * 
			 * RESPONSE SLA BEGIN
			 * 
			 */

			/*
			 * // ackTime is same as crOpenTime and crOpenDateTime, hence slaStartTime for
			 * // Resolution SLA // is OK
			 * 
			 * CAL RESPONSE SLA END TIME
			 * 
			 * if (respondDate.equals(date)) {
			 * 
			 * // if (ws.isHoliday(crRespondDate)) { // logger.info( // crNumber +
			 * " was Responded on " + crRespondDateTime +
			 * " holiday. Proceeding to next day."); // continue; // } // logger.info(date +
			 * " is not a holiday.");
			 * 
			 * if (respondTime.isBefore(bizStartTime)) { slaEndTime = bizStartTime;
			 * logger.info(crNumber + " was Responded before start of Shift hours."); }
			 * 
			 * else if (respondTime.isAfter(bizStartTime) &&
			 * respondTime.isBefore(bizEndTime)) { slaEndTime = respondTime;
			 * logger.info(crNumber + " was Responded in Shift hours."); }
			 * 
			 * else if (respondTime.isAfter(bizEndTime)) { slaEndTime = bizEndTime;
			 * logger.info(crNumber + " was Responded after Shift hours."); }
			 * 
			 * }
			 * 
			 * if (date.isBefore(respondDate) || date.equals(respondDate)) {
			 * logger.info("crRespondDate: " + respondDate + ", date: " + date); if
			 * (slaEndTime == null) { slaEndTime = bizEndTime; }
			 * logger.info("Response SLA calculation will being at: " + slaStartTime);
			 * logger.info("Response SLA calculation will end at: " + slaEndTime);
			 * 
			 * todayRspSeconds = ChronoUnit.SECONDS.between(slaStartTime, slaEndTime);
			 */

			todayRspSeconds = calcResponseTimeForDate(request, date);
			rspSeconds = rspSeconds + todayRspSeconds;

			message.append("\n\t");
			message.append("On " + date + ", time spent in responding (seconds): " + todayRspSeconds + ", "
					+ getHMSfromSeconds(todayRspSeconds));
			// }

			/*
			 * 
			 * RESPONSE SLA END
			 * 
			 */

			// hold sla calculation

			/*
			 * 
			 * SLA HOLD BEGIN WE SHOULD CHECK FOR ALLINSTANCES WHEN TICKET WAS PUT ON
			 * User-Pending or Vendor-Pending etc
			 * 
			 */

//			boolean isSLAOnHold = false;

			/*
			 * 
			 * for (int j = 0; j < slaHoldInstances; j++) { TimePoints holdSlatp =
			 * holdSLAtpList.get(j);
			 * 
			 * LocalDateTime holdStartDateTime = holdSlatp.getStartTime(); LocalDate
			 * holdStartDate = holdStartDateTime.toLocalDate(); LocalTime holdStartTime =
			 * holdStartDateTime.toLocalTime();
			 * 
			 * LocalDateTime holdEndDateTime = holdSlatp.getEndTime(); LocalDate holdEndDate
			 * = holdEndDateTime.toLocalDate(); LocalTime holdEndTime =
			 * holdEndDateTime.toLocalTime();
			 * 
			 * slaStartTime = null; slaEndTime = null;
			 * 
			 * long thisHoldInstanceSeconds = 0; isSLAOnHold = false;
			 * 
			 * 
			 * //START TIME
			 * 
			 * if (holdStartDate.equals(date)) {
			 * 
			 * if (holdStartTime.isBefore(bizStartTime)) { slaStartTime = bizStartTime;
			 * logger.info(crNumber + " was put on SLAHold before start of Shift hours."); }
			 * 
			 * else if (holdStartTime.isAfter(bizStartTime) &&
			 * holdStartTime.isBefore(bizEndTime)) { logger.info(crNumber +
			 * " was put on SLAHold in Shift hours."); slaStartTime = holdStartTime; }
			 * 
			 * else if (holdStartTime.isAfter(bizEndTime)) { logger.info(crNumber +
			 * " was put on SLAHold after Shift hours. Moving to next day."); continue; }
			 * 
			 * logger.info("Hold SLA calculation will begin from: " + slaStartTime);
			 * isSLAOnHold = true; }
			 * 
			 * if (holdStartDate.isBefore(date) && (holdEndDate.equals(date) ||
			 * holdEndDate.isAfter(date))) { slaStartTime = bizStartTime;
			 * logger.info("Hold SLA calculation will begin from: " + slaStartTime);
			 * isSLAOnHold = true; } if (isSLAOnHold) {
			 * logger.info("Hold SLA Start Time and End Time: " + holdStartDateTime + " to "
			 * + holdEndDateTime); }
			 * 
			 * 
			 * CAL HOLD END TIME
			 * 
			 * if (holdEndDate.equals(date)) {
			 * 
			 * if (holdEndTime.isBefore(bizStartTime)) { slaEndTime = bizStartTime;
			 * logger.info(crNumber +
			 * " was removed from SLAHold before start of Shift hours."); }
			 * 
			 * else if (holdEndTime.isAfter(bizStartTime) &&
			 * holdEndTime.isBefore(bizEndTime)) { slaEndTime = holdEndTime;
			 * logger.info(crNumber + " was removed from SLAHold in Shift hours."); }
			 * 
			 * else if (holdEndTime.isAfter(bizEndTime)) { slaEndTime = bizEndTime;
			 * logger.info(crNumber + " was removed from SLAHold after Shift hours."); }
			 * 
			 * }
			 * 
			 * if (isSLAOnHold && (date.isBefore(holdEndDate) || date.equals(holdEndDate)))
			 * { logger.info("CR removed from SLAHold: " + holdEndDate + ", date: " + date);
			 * if (slaEndTime == null) { slaEndTime = bizEndTime; }
			 * logger.info("Hold SLA calculation will being at: " + slaStartTime);
			 * logger.info("Hold SLA calculation will end at: " + slaEndTime);
			 * 
			 * thisHoldInstanceSeconds = ChronoUnit.SECONDS.between(slaStartTime,
			 * slaEndTime); todayHoldSeconds = todayHoldSeconds + thisHoldInstanceSeconds;
			 * holdSeconds = holdSeconds + thisHoldInstanceSeconds;
			 * 
			 * logger.info("Hold Instance [" + (j + 1) + "], " + date +
			 * ", time spent on SLAHold (seconds): " + thisHoldInstanceSeconds + ", " +
			 * getHMSfromSeconds(thisHoldInstanceSeconds)); }
			 * 
			 * } if (isSLAOnHold) { }
			 * 
			 * 
			 * 
			 * SLA HOLD END
			 * 
			 */

			todayHoldSeconds = calcHoldTimeForDate(request, date);
			holdSeconds = holdSeconds + todayHoldSeconds;
			message.append("\n\t");
			message.append("On " + date + ", total time spent on SLAHold (seconds): " + todayHoldSeconds + ", "
					+ getHMSfromSeconds(todayHoldSeconds));

			logger.info("Day Summary: " + message.toString());

			date = date.plusDays(1);
		}

//		logger.info("Overall Summary:\n" 
//			+ "\tResponse/Ack Time Seconds: " + rspSeconds + ", " + getHMSfromSeconds(rspSeconds) 
//			+ "\n\tResolve Time Seconds: " + rslSeconds + ", " + getHMSfromSeconds(rslSeconds) 
//			+ "\n\tHold Time Seconds: " + holdSeconds + ", " + getHMSfromSeconds(holdSeconds)
//		);
		request.setHoldDuration(holdSeconds);
		request.setResolutionDuration(rslSeconds);
		
		if(request.getResponseTPs() != null) {
			request.setResponseDuration(rspSeconds);
		}

		logger.info("Overall Summary:\n" 
				+ "\tResponse/Ack Time Seconds: " + request.getResponseDuration() + ", " + getHMSfromSeconds(request.getResponseDuration()) 
				+ "\n\tResolve Time Seconds: " + request.getResolutionDuration() + ", " + getHMSfromSeconds(request.getResolutionDuration()) 
				+ "\n\tHold Time Seconds: " + request.getHoldDuration() + ", " + getHMSfromSeconds(request.getHoldDuration())
				);
	}

	private long calcHoldTimeForDate(CallRequest cr, LocalDate date) {

		LocalTime slaStartTime = null;
		LocalTime slaEndTime = null;

		Workshift ws = cr.getWorkshift();
		final LocalTime bizStartTime = ws.getShiftStartTimeForDate(date);
		final LocalTime bizEndTime = ws.getShiftEndTimeForDate(date);

		long seconds = 0;

		String crNumber = cr.getRef_num();

//		ArrayList<TimePoints> holdSLAtpList = crHoldSLAMap.get(crNumber);
		ArrayList<TimePoints> holdSLAtpList = cr.getHoldTPList();
		int slaHoldInstances = holdSLAtpList.size();

		for (int j = 0; j < slaHoldInstances; j++) {
			TimePoints holdSlatp = holdSLAtpList.get(j);

			LocalDateTime holdStartDateTime = holdSlatp.getStartTime();
			LocalDate holdStartDate = holdStartDateTime.toLocalDate();
			LocalTime holdStartTime = holdStartDateTime.toLocalTime();

			LocalDateTime holdEndDateTime = holdSlatp.getEndTime();
			LocalDate holdEndDate = holdEndDateTime.toLocalDate();
			LocalTime holdEndTime = holdEndDateTime.toLocalTime();

			slaStartTime = null;
			slaEndTime = null;

			long thisHoldInstanceSeconds = 0;
//			isSLAOnHold = false;

			logger.info("Checking Hold duration for instance " + (j + 1) + " " + "from " + holdStartDateTime + " to "
					+ holdEndDateTime);

			if (date.isBefore(holdStartDate) || date.isAfter(holdEndDate)) {
				Main.logger.info("Date outside Hold start and end dates. Proceeding to next date.");
				continue;
			}

			/*
			 * START TIME
			 */
			if (holdStartDate.equals(date)) {

				if (holdStartTime.isBefore(bizStartTime)) {
					slaStartTime = bizStartTime;
					logger.info(crNumber + " was put on SLAHold before start of Shift hours.");
				}

				else if (holdStartTime.isAfter(bizStartTime) && holdStartTime.isBefore(bizEndTime)) {
					logger.info(crNumber + " was put on SLAHold in Shift hours.");
					slaStartTime = holdStartTime;
				}

				else if (holdStartTime.isAfter(bizEndTime)) {
					logger.info(crNumber + " was put on SLAHold after Shift hours. Moving to next day.");
					slaStartTime = bizEndTime;
				}

			}

			/*
			 * END TIME
			 */
			if (holdEndDate.equals(date)) {

				if (holdEndTime.isBefore(bizStartTime)) {
					slaEndTime = bizStartTime;
					logger.info(crNumber + " was removed from SLAHold before start of Shift hours.");
				}

				else if (holdEndTime.isAfter(bizStartTime) && holdEndTime.isBefore(bizEndTime)) {
					slaEndTime = holdEndTime;
					logger.info(crNumber + " was removed from SLAHold in Shift hours.");
				}

				else if (holdEndTime.isAfter(bizEndTime)) {
					slaEndTime = bizEndTime;
					logger.info(crNumber + " was removed from SLAHold after Shift hours.");
				}

			}

			if (slaStartTime == null) {
				slaStartTime = bizStartTime;
			}

			if (slaEndTime == null) {
				slaEndTime = bizEndTime;
			}

			logger.info("Hold SLA calculation will being at: " + slaStartTime);
			logger.info("Hold SLA calculation will end at: " + slaEndTime);

			thisHoldInstanceSeconds = ChronoUnit.SECONDS.between(slaStartTime, slaEndTime);
			seconds = seconds + thisHoldInstanceSeconds;
			seconds = adjustOneSecond(seconds);

			logger.info("Hold Instance [" + (j + 1) + "], " + date + ", time spent on SLAHold (seconds): "
					+ thisHoldInstanceSeconds + ", " + getHMSfromSeconds(thisHoldInstanceSeconds));
		}

		return seconds;

	}

	private long adjustOneSecond(long seconds) {
		return seconds==86399? 86400L: seconds;
	}

	private long calcResolutionTimeForDate(CallRequest cr, LocalDate date) {

//		LocalDateTime rslStartDateTime = crRslSLAMap.get(cr.getId()).getStartTime();
//		LocalTime resolveStartTime = rslStartDateTime.toLocalTime();
//		LocalDate resolveStartDate = rslStartDateTime.toLocalDate();
		
		LocalDateTime resolveStartDateTime = cr.getResolutionTPs().getStartTime();
		LocalTime resolveStartTime = resolveStartDateTime.toLocalTime();
		LocalDate resolveStartDate = resolveStartDateTime.toLocalDate();

//		LocalDateTime resolveEndDateTime = crRslSLAMap.get(cr.getId()).getEndTime();
//		LocalTime resolveEndTime = resolveEndDateTime.toLocalTime();
//		LocalDate resolveEndDate = resolveEndDateTime.toLocalDate();

		LocalDateTime resolveEndDateTime = cr.getResolutionTPs().getEndTime();
		LocalTime resolveEndTime = resolveEndDateTime.toLocalTime();
		LocalDate resolveEndDate = resolveEndDateTime.toLocalDate();
		
		LocalTime slaStartTime = null;
		LocalTime slaEndTime = null;

		long seconds = 0;

		Workshift ws = cr.getWorkshift();

		final LocalTime bizStartTime = ws.getShiftStartTimeForDate(date);
		final LocalTime bizEndTime = ws.getShiftEndTimeForDate(date);

		Main.logger.info("Checking Resolution dates: " + resolveStartDateTime + ", to " + resolveEndDateTime);

		if (date.isBefore(resolveStartDate) || date.isAfter(resolveEndDate)) {
			Main.logger.info("Date outside Resolution start and end dates. Proceeding to next date.");
			return seconds;
		}

		if (resolveStartDate.equals(date)) {

//			if (ws.isHoliday(crOpenDate)) {
//				logger.info(crNumber + " was Opened on " + crOpenDateTime + " holiday. Moving to next day.");
//				continue;
//			}
//			logger.info(date + " is not a holiday.");

			if (resolveStartTime.isBefore(bizStartTime)) {
				slaStartTime = bizStartTime;
				logger.info(cr.getRef_num() + " was opened before start of Shift hours.");
			}

			else if (resolveStartTime.isAfter(bizStartTime) && resolveStartTime.isBefore(bizEndTime)) {
				logger.info(cr.getRef_num() + " was opened in Shift hours.");
				slaStartTime = resolveStartTime;
			}

			else if (resolveStartTime.isAfter(bizEndTime)) {
				slaStartTime = bizEndTime;
				logger.info(cr.getRef_num() + " was opened after Shift hours. Moving to next day.");
			}
		}

		/*
		 * CALC END TIME
		 */

		if (resolveEndDate.equals(date)) {

			if (resolveEndTime.isBefore(bizStartTime)) {
				slaEndTime = bizStartTime;
				logger.info(cr.getRef_num() + " was Resolved before Shift hours.");
			}

			else if (resolveEndTime.isAfter(bizStartTime) && resolveEndTime.isBefore(bizEndTime)) {
				slaEndTime = resolveEndTime;
				logger.info(cr.getRef_num() + " was Resolved in Shift hours.");
			}

			else if (resolveEndTime.isAfter(bizEndTime)) {
				slaEndTime = bizEndTime;
				logger.info(cr.getRef_num() + " was Resolved after Shift hours.");
			}

		}

		if (slaStartTime == null) {
			slaStartTime = bizStartTime;
		}

		if (slaEndTime == null) {
			slaEndTime = bizEndTime;
		}
		logger.info("Resolution SLA calculation start time: " + slaStartTime);
		logger.info("Resolution SLA calculation end time: " + slaEndTime);

		seconds = ChronoUnit.SECONDS.between(slaStartTime, slaEndTime);
		seconds = adjustOneSecond(seconds);

		return seconds;
	}

	private long calcResponseTimeForDate(CallRequest cr, LocalDate date) {

		long seconds = 0;
//		LocalDateTime responseStartDateTime = crRspSLAMap.get(cr.getId()).getStartTime();
		
		if(cr.getResponseTPs() == null) {
			Main.logger.warning("Skipping Response Time calculation as Response Times unavailable");
			return seconds;
		}
		
		LocalDateTime responseStartDateTime = cr.getResponseTPs().getStartTime();
		LocalTime responseStartTime = responseStartDateTime.toLocalTime();
		LocalDate responseStartDate = responseStartDateTime.toLocalDate();

//		LocalDateTime responseEndDateTime = crRspSLAMap.get(cr.getId()).getEndTime();
		LocalDateTime responseEndDateTime = cr.getResponseTPs().getEndTime();
		LocalTime responseEndTime = responseEndDateTime.toLocalTime();
		LocalDate responseEndDate = responseEndDateTime.toLocalDate();

		LocalTime slaStartTime = null;
		LocalTime slaEndTime = null;

		Workshift ws = cr.getWorkshift();

		final LocalTime bizStartTime = ws.getShiftStartTimeForDate(date);
		final LocalTime bizEndTime = ws.getShiftEndTimeForDate(date);
		
		Main.logger.info("Checking Response dates: " + responseStartDate + ", to " + responseEndDate);


		if (date.isBefore(responseStartDate) || date.isAfter(responseEndDate)) {
			Main.logger.info("Date outside Response start and end dates. Proceeding to next date.");
			return seconds;
		}

		if (responseStartDate.equals(date)) {

			if (responseStartTime.isBefore(bizStartTime)) {
				logger.info(cr.getRef_num() + " was opened before Shift hours.");
				slaStartTime = bizStartTime;
			}

			else if (responseStartTime.isAfter(bizStartTime) && responseStartTime.isBefore(bizEndTime)) {
				slaStartTime = responseStartTime;
				logger.info(cr.getRef_num() + " was opened in Shift hours.");
			}

			else if (responseStartTime.isAfter(bizEndTime)) {
				slaStartTime = bizEndTime;
				logger.info(cr.getRef_num() + " was opened after Shift hours. Moving to next day.");
			}
		} else {
			Main.logger.info("INSIDE START ELSE");
		}

		/*
		 * CALC END TIME
		 */

		if (responseEndDate.equals(date)) {
			logger.finer("1");

			if (responseEndTime.isBefore(bizStartTime)) {
				logger.finer("2");
				slaEndTime = bizStartTime;
				logger.info(cr.getRef_num() + " was Responded before Shift hours.");
			}

			else if (responseEndTime.isAfter(bizStartTime) && responseEndTime.isBefore(bizEndTime)) {
				logger.finer("3");
				slaEndTime = responseEndTime;
				logger.info(cr.getRef_num() + " was Responded in Shift hours.");
			}

			else if (responseEndTime.isAfter(bizEndTime)) {
				logger.finer("4");
				slaEndTime = bizEndTime;
				logger.info(cr.getRef_num() + " was Responded after Shift hours.");
			}
		}

		if (slaStartTime == null) {
			slaStartTime = bizStartTime;
		}
		if (slaEndTime == null) {
			slaEndTime = bizEndTime;
		}

		logger.info("Response SLA calculation start time: " + slaStartTime);
		logger.info("Response SLA calculation end time: " + slaEndTime);

		seconds = ChronoUnit.SECONDS.between(slaStartTime, slaEndTime);
		seconds = adjustOneSecond(seconds);

		return seconds;

	}

	private String getHMSfromSeconds(long seconds) {
		
		if(seconds < 0) {
			return "NA";
		}

		long sRem = seconds % 60;
		long min = (seconds - sRem) / 60;
		long minRem = min % 60;
		long hr = (min - minRem) / 60;
		long hrRem = hr % 24;
		long day = (hr - hrRem) / 24;
//		logger.fine(String.valueOf("Rem: " + sRem));
//		logger.fine(String.valueOf("Min: " + min));
//		logger.fine(String.valueOf("Min R: " + min));
//		logger.fine(String.valueOf("Hr: " + hr));
		return (day > 0 ? String.valueOf(day) + ":" : "") + String.format("%02d", hrRem) + ":"
				+ String.format("%02d", minRem) + ":" + String.format("%02d", sRem);
	}

	private void processActLogs(CallRequest cr) {

		actLogHelper = new ActivityLogHelper(cr);
		actLogHelper.process();

	}

	/*private void prepareSLATimePoints(CallRequest cr) {

		crRspSLAMap = actLogHelper.getCrRspSLAMap();
		crRslSLAMap = actLogHelper.getCrRslSLAMap();
		crHoldSLAMap = actLogHelper.getCrHoldSLAMap();

	}*/

}
