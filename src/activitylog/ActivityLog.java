package activitylog;

import java.time.LocalDateTime;

public class ActivityLog {

	LocalDateTime timestamp;
	String type;
	String actionDesc;
	String crId;
	
	public ActivityLog(LocalDateTime timestamp, String type, String actionDesc, String crId) {
		this.timestamp = timestamp;
		this.type = type;
		this.actionDesc = actionDesc;
		this.crId = crId;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public String getType() {
		return type;
	}

	public String getActionDesc() {
		return actionDesc;
	}

	public String getCrId() {
		return crId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ActivityLog [timestamp=");
		builder.append(timestamp);
		builder.append(", type=");
		builder.append(type);
		builder.append(", actionDesc=");
		builder.append(actionDesc);
		builder.append(", crId=");
		builder.append(crId);
		builder.append("]");
		return builder.toString();
	}
		
	
}
