package activitylog;

import java.time.LocalDateTime;

public class TimePoints {

	private final LocalDateTime startTime;
	private final LocalDateTime endTime;
	private final String name;
	
	public TimePoints(String name, LocalDateTime startTime, LocalDateTime endTime) {
		this.name = name;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("name=");
		builder.append(name);
		builder.append(", SLATimePoints [startTime=");
		builder.append(startTime);
		builder.append(", endTime=");
		builder.append(endTime);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
