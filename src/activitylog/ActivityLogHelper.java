package activitylog;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import callrequest.StatusHelper;
import main.Main;
import callrequest.CallRequest;
import callrequest.Status;

public class ActivityLogHelper {

	private ArrayList<ActivityLog> logList = new ArrayList<>();

	private String crNumber = null;
	private CallRequest cr;

	private HashMap<String, TimePoints> crRspSLAMap = new HashMap<>();
	private HashMap<String, TimePoints> crRslSLAMap = new HashMap<>();
	private HashMap<String, ArrayList<TimePoints>> crHoldSLAMap = new HashMap<>();

	public ActivityLogHelper(CallRequest cr) {
		this.cr = cr;
	}

	public HashMap<String, TimePoints> getCrRspSLAMap() {
		return crRspSLAMap;
	}

	public HashMap<String, TimePoints> getCrRslSLAMap() {
		return crRslSLAMap;
	}

	public HashMap<String, ArrayList<TimePoints>> getCrHoldSLAMap() {
		return crHoldSLAMap;
	}

	public void process() {

//		logList = this.get();
		logList = ActivityLogDAO.getInstance().getAllLogs(cr.getRef_num());

		LocalDateTime tOpen = null; // ticket Open Time (only once)
		LocalDateTime tStart = null; // Ticket Re-Open Time (can be multiple, consider last value to calc resolution sla)
		LocalDateTime tAck = null;
		LocalDateTime tResolve = null;
		LocalDateTime tClose = null;
		LocalDateTime tHoldStart = null;
		LocalDateTime tHoldEnd = null;
		TimePoints rspSLAtp = null;
		TimePoints rslSLAtp = null;
		ArrayList<TimePoints> holdSLAtpList = new ArrayList<>();
		boolean isHold = false;
		boolean isResolved = false;

		logList.sort((x, y) -> (int) (x.getTimestamp().atZone(ZoneOffset.systemDefault()).toEpochSecond()
				- y.getTimestamp().atZone(ZoneOffset.systemDefault()).toEpochSecond()));

		/*
		 * logList should be sorted based on timestamp field
		 */
		for (ActivityLog log : logList) {

			Main.logger.info("Inspecting log: " + log.getType() + ": " + log.getActionDesc());
			/*
			 * INIT: during creation of ticket. this happens only once.
			 */
			if (log.getType().equals("INIT")) {
				Main.logger.info("INIT log");
				tOpen = log.getTimestamp();
				tStart = tOpen;
			}
			
			/*
			 *  ESC: during re-prioritization of ticket, as New SLA is applied and should be calculated
			 * from that time onwards.
			 */

			if (log.getType().equals("ESC")) {
				Main.logger.info("ESCALATE log");
				tStart = log.getTimestamp();
			}
			
			/*
			 * if (log.getType().equals("ST") &&
			 * log.getActionDesc().endsWith("'Acknowledge'")) { Main.logger.info("Ack log");
			 * tAck = log.getTimestamp(); if (rspSLAtp == null) { rspSLAtp = new
			 * SLATimePoints("RSP", tOpen, tAck); } }
			 */

			/*
			 * TODO how to confirm if Acknowledge is the Resposne sla status, and no other.
			 */
			if (log.getType().equals("ST") && log.getActionDesc().endsWith("'Acknowledged'")) {
				Main.logger.info("ACKNOWLEDGE LOG");
				tAck = log.getTimestamp();
				if (rspSLAtp == null) {
					rspSLAtp = new TimePoints("RSP", tOpen, tAck);
				}
			}

			/*
			 * RE can come more than once in act_log, in re-open cases. 1. when ticket goes
			 * to Resolve, along with RE, there is also SLADELAY log SLADELAY is sla hold
			 * status.
			 */
			if (log.getType().equals("RE")) {
				isResolved = true;
				Main.logger.info("RESOLVE LOG");
				tResolve = log.getTimestamp();
				rslSLAtp = new TimePoints("RSL", tStart, tResolve);
				/*
				 * For scenario when status changes from 'User Pending' to 'Technically
				 * Resolved'
				 */
				if (isHold) {
					Main.logger.info("\tHOLD ENDS HERE");
					tHoldEnd = log.getTimestamp();
					TimePoints tp = new TimePoints("HOLD", tHoldStart, tHoldEnd);
					holdSLAtpList.add(tp);
					isHold = false;
				}
			}

			if (log.getType().equals("SLADELAY")) {
				isHold = true;
				Main.logger.fine("SLADELAY LOG, Hold Start");
				tHoldStart = log.getTimestamp();
			}

			if (log.getType().equals("SLARESUME")) {
				Main.logger.fine("SLARESUME LOG, Hold End");
				/*
				 * For the scenario when ticket is Reopened, then Closed directly without moving to RE.
				 */
				if (isHold && isResolved) {
					Main.logger.fine("This is a reopen case. Setting isResolved to false.");
					isResolved = false;
				}
				if (isHold) {
					Main.logger.fine("Was on hold earlier");
					tHoldEnd = log.getTimestamp();
					TimePoints tp = new TimePoints("HOLD", tHoldStart, tHoldEnd);
					holdSLAtpList.add(tp);
					isHold = false;
				}

				
			}
			/*
			 * For the scenario when ticket was RE earlier, then reopened and made CL directly.
			 */
			if (log.getType().equals("CL")) {
				Main.logger.fine("CLOSED LOG");
				if (!isResolved) {
					Main.logger.fine("Consdering CL log Resolution time, in absense of RE log");
					tClose = log.getTimestamp();
					rslSLAtp = new TimePoints("RSL", tStart, tClose);
				}
			}

			/*
			 * if (log.getType().equals("ST") && hasHoldStarted(log)) {
			 * Main.logger.info("User-Pending start"); userPending = true; tSlaHoldStart =
			 * log.getTimestamp(); }
			 * 
			 * if ((log.getType().equals("ST") || log.getType().equals("RE")) &&
			 * log.getActionDesc().contains("'User-Pending' to")) {
			 * Main.logger.info("User-Pending end"); tHoldEnd = log.getTimestamp(); if
			 * (isHold) { SLATimePoints tp = new SLATimePoints("HOLD", tHoldStart,
			 * tHoldEnd); holdSLAtpList.add(tp); isHold = false; } }
			 */

		}

//		this.crRspSLAMap.put(crNumber, rspSLAtp);
//		this.crRslSLAMap.put(crNumber, rslSLAtp);
//		this.crHoldSLAMap.put(crNumber, holdSLAtpList);
		
		cr.setResponseTPs(rspSLAtp);
		cr.setResolutionTPs(rslSLAtp);
		cr.setHoldTPList(holdSLAtpList);
		

		Main.logger.info("Processing Results:");

		try {
			Main.logger.info(rspSLAtp.toString());
		} catch (Exception e) {
			Main.logger.warning("Response SLA TimePoint is null");
		}
		try {
			Main.logger.info(rslSLAtp.toString());
		} catch (Exception e) {
			Main.logger.warning("Resolution SLA TimePoint is null");
		}
		try {
			Main.logger.info(holdSLAtpList.toString());
		} catch (Exception e) {
			Main.logger.warning("Hold SLA TimePoints are null");
		}
	}

	/*
	 * public boolean hasHoldStarted(ActivityLog log) { return
	 * StatusHelper.getStatusSet().stream().filter(s -> s.isHold()) .anyMatch(status
	 * -> log.getActionDesc().endsWith("'" + status.getName() + "'")); }
	 */

	private ArrayList<ActivityLog> get() {

//		ActivityLog log1 = new ActivityLog(LocalDateTime.of(2018, 10, 12, 13, 10, 0), "INIT", "Ticket created",
//				"CR1234");
//		ActivityLog log2 = new ActivityLog(LocalDateTime.of(2018, 10, 12, 13, 14, 0), "ST",
//				"Status changed from 'Open' to 'Acknowledged'", "CR1234");
//
//		ActivityLog log3 = new ActivityLog(LocalDateTime.of(2018, 10, 12, 13, 30, 0), "ST",
//				"Status changed from 'Acknowledged' to 'Work In Progress'", "CR1234");
//
//		ActivityLog log4 = new ActivityLog(LocalDateTime.of(2018, 10, 13, 10, 0, 0), "ST",
//				"Status changed from 'Work In Progress' to 'User Pending'", "CR1234");
//
//		ActivityLog log41 = new ActivityLog(LocalDateTime.of(2018, 10, 13, 10, 0, 0), "SLADELAY",
//				"Status changed from 'Work In Progress' to 'User Pending'", "CR1234");
//
//		ActivityLog log5 = new ActivityLog(LocalDateTime.of(2018, 10, 13, 11, 0, 0), "RE",
//				"Status changed from 'User Pending' to 'Technically Closed'", "CR1234");
//
//		/*
//		 * type "CL" = closed. if RE is not found, use CL status for ticket end time.
//		 */
//
//		logList.add(log3);
//		logList.add(log4);
//		logList.add(log2);
//		logList.add(log1);
//		logList.add(log41);
//		logList.add(log5);
		
		ActivityLog log1 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769360, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "INIT", "New ticket created", "cr:123");
		ActivityLog log2 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769420, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "ST", "Status changed from 'Open' to 'Acknowledged'", "cr:123");
		ActivityLog log3 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769428, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "ST", "Status changed from 'Acknowledged' to 'Work In Progress'", "cr:123");
		ActivityLog log4 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769431, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "ST", "Status changed from 'Work In Progress' to 'User-Pending'", "cr:123");
		ActivityLog log5 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769460, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "SLADELAY", "The service type on ticket were delayed", "cr:123");
		ActivityLog log6 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769467, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "ST", "Status changed from 'User-Pending' to 'Work In Progress'", "cr:123");
		ActivityLog log7 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769487, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "SLARESUME", "SErvice has resumed", "cr:123");
		ActivityLog log8 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769491, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "RE", "Status changed from 'Work In Progress' to 'Technically Closed'", "cr:123");
		
		ActivityLog log9 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769520, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "SLADELAY", "Service type were delayed", "cr:123");
		ActivityLog log10 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769688, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "ST", "Status changed from 'Technically Closed' to 'Reopened'", "cr:123");
		ActivityLog log11 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769693, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "SLARESUME", "Service type resumed", "cr:123");
		ActivityLog log12 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539769693, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "ST", "Status changed from 'Reopened' to 'Work In Progress'", "cr:123");
		ActivityLog log13 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539770102, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "RE", "Status changed from 'Work In Progress' to 'Technically Closed", "cr:123");
		ActivityLog log14 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539770147, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "SLADELAY", "Service type delayed.", "cr:123");
		ActivityLog log15 = new ActivityLog(
				LocalDateTime.ofEpochSecond(1539770162, 0, ZoneOffset.ofHoursMinutes(5, 30))
				, "CL", "Closed", "cr:123");
		
		logList.add(log1);
		logList.add(log2);
		logList.add(log3);
		logList.add(log4);
		logList.add(log5);
		logList.add(log6);
		logList.add(log7);
		logList.add(log8);
		logList.add(log9);
		logList.add(log10);
		logList.add(log11);
		logList.add(log12);
		logList.add(log13);
		logList.add(log14);
		logList.add(log15);
		
		return logList;
	}
}
