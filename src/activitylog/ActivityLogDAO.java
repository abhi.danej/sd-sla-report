package activitylog;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;

import main.Main;

public class ActivityLogDAO {
	
	/*
	 * store ActivityLog as Map < crNumber, Collection<ActivityLog> > using Multimap
	 * This will help achieve multi-threading later
	 * https://github.com/google/guava/wiki/NewCollectionTypesExplained#multimap
	 */
	
	private ArrayList<ActivityLog> logList; 
	
	private static ActivityLogDAO alDAO = new ActivityLogDAO();
	
	private ActivityLogDAO() {
		
	}
	
	public static ActivityLogDAO getInstance() {
		return alDAO;
	}
	
	public ArrayList<ActivityLog> getAllLogs(String ref_num) {
		
		logList = new ArrayList<>();

		try {
			Main.logger.info("called");
			String connectionURL = "jdbc:sqlserver://HBUSDUATDB_NEW:1400;database=mdb;user=servicedesk;password=wipro@123";
			
			String sql = "Select "
					+ " time_stamp, type, action_desc, call_req_id "
					+ " from act_log"
					+ " where call_req_id in (select persid from call_req where ref_num='" + ref_num + "')"
					+ " order by id";
//			String sql = "Select * from zadd_call_req where zdel_flag=0 order by id asc";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(connectionURL);
			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(sql);
			Main.logger.info("SQL Select command executed successfully and return the resultset.");
			
			while(rs.next())
			{
/*				Main.logger.info(
							rs.getString(1) + ": " 
							+ rs.getString(2) + ": " 
							+ rs.getString(3) + ": " 
							+ rs.getString(4) + ": "
							);
*/				
				ActivityLog log = new ActivityLog(
						LocalDateTime.ofEpochSecond(rs.getLong(1), 0, ZoneOffset.ofHoursMinutes(5, 30))
						, rs.getString(2), rs.getString(3), rs.getString(4));
				logList.add(log);
			}
			rs.close();
			con.close();
			
		} catch (Exception e) {
			Main.logger.info(e.getMessage());
			Main.logger.info("SQL Select command execution failed, Reason - "
					+ e.getMessage());
	
		}
		

		return logList;

	}

}
