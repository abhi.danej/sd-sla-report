package commons;

//import java.text.SimpleDateFormat;
//import java.time.LocalDateTime;
//import java.util.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.LogRecord;

public class LogFormat extends java.util.logging.Formatter {

//	@Override
	public String format(LogRecord record) {
//		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YYYY HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
				
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM YYYY HH:mm:ss");
		
        String fNow = now.format(formatter);

		return
				fNow 
				+ " " 
				+record.getLevel().toString().substring(0,4)+ " " 
				+record.getSourceClassName()+ " " 
				+record.getMessage() +
				"\n";
	}

}
