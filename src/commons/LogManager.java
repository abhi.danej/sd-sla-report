package commons;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
//import java.util.logging.*;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class LogManager {
	private static Logger logger = Logger.getLogger(LogManager.class.getSimpleName());

	public Logger getLogger(Properties prop) throws Exception, SecurityException, IOException {
		
//		String logPath = prop.getProperty("adapter.log.home") + "adapter%g.txt";
//		int maxSizeMB = Integer.parseInt(prop.getProperty("adapter.log.size.mb"));
//		Level level = Level.parse(prop.getProperty("adapter.log.level"));

//		Handler fileHandler = new FileHandler(
//				logPath, 
//				1024*1024*maxSizeMB, 
//				3, true);
		
//		fileHandler.setFormatter(new LogFormat());
		
//		logger.setLevel(level);
//		logger.addHandler(fileHandler);
		
		logger.setLevel(Level.ALL);
		Handler consoleHandler = new ConsoleHandler();
		consoleHandler.setFormatter(new LogFormat());
		consoleHandler.setLevel(Level.ALL);
		logger.addHandler(consoleHandler);
//		Stream.of(logger.getHandlers()).forEach(
//				x -> System.out.println(x.toString()));
		logger.setUseParentHandlers(false);
		return logger;
	}

}
