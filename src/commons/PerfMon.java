package commons;

import main.Main;

public class PerfMon {

	public static void getMemStats() {
		Runtime r = Runtime.getRuntime();
		long rMem = 1024L * 1024L;
		long memMB = (r.totalMemory() - r.freeMemory())/rMem;
		Main.logger.info("Memory in use: " + memMB + " MB");
	}
}
